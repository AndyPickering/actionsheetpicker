//
//  ActionSheetDecimalPicker.m
//
//  Created by Koji Hasegawa on 12/11/22.
//

//ActionSheetPicker:
//
//Copyright (c) 2011, Tim Cinel
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions are met:
//* Redistributions of source code must retain the above copyright
//notice, this list of conditions and the following disclaimer.
//* Redistributions in binary form must reproduce the above copyright
//notice, this list of conditions and the following disclaimer in the
//documentation and/or other materials provided with the distribution.
//* Neither the name of the <organization> nor the
//names of its contributors may be used to endorse or promote products
//derived from this software without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//åLOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "ActionSheetDecimalPicker.h"
#import "DistancePickerView.h"
#import <objc/message.h>


@interface ActionSheetDecimalPicker()
@property (nonatomic, assign) NSInteger digits;
@property (nonatomic, assign) NSInteger maxIntValue;
@property (nonatomic, assign) NSInteger selectedIntValue;
//@property (nonatomic, retain) NSNumber *maxValue;
//@property (nonatomic, retain) NSNumber *selectedValue;
//@property (nonatomic, assign) NSInteger flatValue;
@property (nonatomic, assign) NSInteger flatValueLength;
@end

@implementation ActionSheetDecimalPicker
@synthesize digits = _digits;
@synthesize maxIntValue = _maxIntValue;
@synthesize selectedIntValue = _selectedIntValue;
//@synthesize flatValue = _flatValue;
@synthesize flatValueLength = _flatValueLength;

static NSString *PERIOD = @". ";

+ (id)showPickerWithTitle:(NSString *)title digits:(NSInteger)digits maxIntValue:(NSInteger)maxIntValue selectedIntValue:(NSInteger)selectedIntValue target:(id)target action:(SEL)action origin:(id)origin{
    if(maxIntValue < selectedIntValue) {
        maxIntValue = selectedIntValue;
    }
    
    ActionSheetDecimalPicker *picker = [[ActionSheetDecimalPicker alloc] initWithTitle:title digits:digits maxIntValue:maxIntValue selectedIntValue:selectedIntValue target:target action:action origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

+ (id)showPickerWithTitle:(NSString *)title digits:(NSInteger)digits selectedIntValue:(NSInteger)selectedIntValue target:(id)target action:(SEL)action origin:(id)origin {
    NSInteger intLength = [[NSString stringWithFormat:@"%i", selectedIntValue] length];
    NSInteger mostSignificantDigitValue = selectedIntValue / (int)pow((double)10, (double)(intLength - 1));
    NSInteger maxIntValue = (mostSignificantDigitValue+1) * pow((double)10, (double)(intLength - 1));
    
    ActionSheetDecimalPicker *picker = [[ActionSheetDecimalPicker alloc] initWithTitle:title digits:digits maxIntValue:maxIntValue selectedIntValue:selectedIntValue target:target action:action origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

- (id)initWithTitle:(NSString *)title digits:(NSInteger)digits maxIntValue:(NSInteger)maxIntValue selectedIntValue:(NSInteger)selectedIntValue target:(id)target action:(SEL)action origin:(id)origin {
    self = [super initWithTarget:target successAction:action cancelAction:nil origin:origin];
    if (self) {
        self.title = title;
        self.digits = digits;
        self.maxIntValue = maxIntValue;
        self.selectedIntValue = selectedIntValue;
        self.flatValueLength = [[NSString stringWithFormat:@"%i", maxIntValue] length];
        if(_flatValueLength<(_digits+1))
            _flatValueLength = _digits+1;
    }
    return self;
}


- (UIView *)configuredPickerView {
    CGRect decimalPickerFrame = CGRectMake(0, 40, self.viewSize.width, 216);
    DistancePickerView *picker = [[DistancePickerView alloc] initWithFrame:decimalPickerFrame];
    picker.delegate = self;
    picker.dataSource = self;
    picker.showsSelectionIndicator = YES;
    
    if(_digits>0){
        NSInteger periodPosition = _flatValueLength - _digits - 1;
        [picker addLabel:PERIOD forComponent:periodPosition forLongestString:nil];
    }
    
    NSInteger unitSubtract = 0;
    NSInteger currentDigit = 0;
    
    for (int i = 0; i < self.flatValueLength; ++i) {
        NSInteger factor = (int)pow((double)10, (double)(self.flatValueLength - (i+1)));
        currentDigit = (( self.selectedIntValue - unitSubtract ) / factor )  ;
        [picker selectRow:currentDigit inComponent:i animated:NO];
        unitSubtract += currentDigit * factor;
    }
    
    //need to keep a reference to the picker so we can clear the DataSource / Delegate when dismissing
    self.pickerView = picker;
    
    return picker;
}

- (void)notifyTarget:(id)target didSucceedWithAction:(SEL)action origin:(id)origin {
    NSInteger newFlatValue = 0;
    DistancePickerView *picker = (DistancePickerView *)self.pickerView;
    for (int i = 0; i < self.flatValueLength; ++i)
        newFlatValue += [picker selectedRowInComponent:i] * (int)pow((double)10, (double)(self.flatValueLength - (i + 1)));
    
    //sending three objects, so can't use performSelector:
    if ([target respondsToSelector:action])
        objc_msgSend(target, action, newFlatValue, origin);
    else
        NSAssert(NO, @"Invalid target/action ( %s / %s ) combination used for ActionSheetPicker", object_getClassName(target), sel_getName(action));
}

#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.flatValueLength;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0){
        NSInteger mostSignificantDigitValue = _maxIntValue / (int)pow((double)10, (double)(_flatValueLength - 1));
        return mostSignificantDigitValue + 1;
    }
    return 10;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
     return [NSString stringWithFormat:@"%i", row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGFloat totalWidth = pickerView.frame.size.width - 30;
    CGFloat digitPointSize = [PERIOD sizeWithFont:[UIFont boldSystemFontOfSize:20]].width;
    CGFloat otherSize = (totalWidth - digitPointSize)/self.flatValueLength;
    
    if(_digits>0){
        NSInteger periodPosition = _flatValueLength - _digits - 1;
        if (component == periodPosition)
            return otherSize + digitPointSize;
    }
    return otherSize;
}


- (void)customButtonPressed:(id)sender {
    NSLog(@"Not implemented. If you get around to it, please contribute back to the project :)");
}

@end
